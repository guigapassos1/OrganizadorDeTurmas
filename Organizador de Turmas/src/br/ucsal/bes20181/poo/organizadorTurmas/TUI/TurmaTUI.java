package br.ucsal.bes20181.poo.organizadorTurmas.TUI;

import java.util.Scanner;

import br.ucsal.bes20181.poo.organizadorTurmas.DOMAIN.TurmaDOMAIN;

public class TurmaTUI {

	static Scanner sc = new Scanner(System.in);
	static TurmaDOMAIN turma = new TurmaDOMAIN(null, 0, false, null, null,0);

	public void TurmaRegistro() {
		System.out.println("-----REGISTRO DE TURMA-----");
		System.out.println("Nome da Matéria:");
		turma.setMateria(sc.next());
		System.out.println("Quantidade de alunos:");
		turma.setQtdAlunos(sc.nextInt());
		System.out.println("Professor está disponivel?(S/N)");
		turma.setDispProfessor(sc.nextBoolean());
		System.out.println("Data de início:");
		turma.setDataInicio(sc.next());
		System.out.println("Data de fim:");
		turma.setDataFim(sc.next());
		System.out.println("N�mero da turma:");
		turma.setNumTurma(sc.nextInt());
		System.out.println("-----Turma-----");
		System.out.println("Matéria: " + turma.getMateria() + "\nQuantidade de alunos: " + turma.getQtdAlunos()
				+ "\nProfessor disponivel: " + turma.getDispProfessor() + "\nData de início: " + turma.getDataInicio()
				+ "\nData de fim: " + turma.getDataFim()+ "\nN�mero da turma: " + turma.getNumTurma());
		System.out.println("-----Turma-----");
	}
}