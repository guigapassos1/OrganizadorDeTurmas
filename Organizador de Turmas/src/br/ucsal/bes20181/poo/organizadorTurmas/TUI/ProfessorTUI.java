package br.ucsal.bes20181.poo.organizadorTurmas.TUI;

import java.util.Scanner;

import br.ucsal.bes20181.poo.organizadorTurmas.DOMAIN.ProfessorDOMAIN;

public class ProfessorTUI {

	static Scanner sc = new Scanner(System.in);
	static ProfessorDOMAIN professor = new ProfessorDOMAIN(null, null, null);

	public void ProfessorRegistro() {
		System.out.println("-----REGISTRO DO PROFESSOR-----");
		System.out.println("Nome do Professor:");
		professor.setNome(sc.next());
		System.out.println("E-mail do Professor:");
		professor.setEmail(sc.next());
		System.out.println("Matéria do Professor:");
		professor.setMateria(sc.next());
		System.out.println("-----Professor-----");
		System.out.println("Nome: " + professor.getNome() + "\nE-mail: " + professor.getEmail() + "\nMatéria: "
				+ professor.getMateria());
		System.out.println("-----Professor-----");
	}
}