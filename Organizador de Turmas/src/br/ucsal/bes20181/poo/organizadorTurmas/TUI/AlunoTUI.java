package br.ucsal.bes20181.poo.organizadorTurmas.TUI;

import java.util.Scanner;

import br.ucsal.bes20181.poo.organizadorTurmas.BO.AlunoBO;
import br.ucsal.bes20181.poo.organizadorTurmas.DOMAIN.AlunoDOMAIN;

public class AlunoTUI {
	
	Scanner sc = new Scanner(System.in);
	AlunoDOMAIN alunoDOMAIN = new AlunoDOMAIN();
	
	AlunoBO alunoBO = new AlunoBO();

	public void alunoRegistro() {
		System.out.println("-----DESCREVA O ALUNO-----");
		definirNome();
		definirEmail();
		definirMatricula();
		definirTelefone();
		imprimir();
	}

	private void imprimir() {
		System.out.println("-----Aluno-----");
		System.out.println("Nome: " + alunoDOMAIN.getNome() + "\nE-mail: " + alunoDOMAIN.getEmail() + "\nMatricula: "
				+ alunoDOMAIN.getMatricula() + "\nTelefone: " + alunoDOMAIN.getTelefone());
		System.out.println("-----Aluno-----");
	}

	private void definirTelefone() {
		System.out.println("Telefone do Aluno:");
		alunoDOMAIN.setTelefone(sc.next());
	}

	private void definirMatricula() {
		System.out.println("Matricula do Aluno:");
		alunoDOMAIN.setMatricula(sc.nextInt());
	}

	private void definirEmail() {
		System.out.println("E-mail do Aluno:");
		alunoDOMAIN.setEmail(sc.next());
		alunoBO.verificarEmail(alunoDOMAIN);
	}

	private void definirNome() {
		System.out.println("Nome do Aluno:");
		alunoDOMAIN.setNome(sc.next());
		alunoBO.verificarNome(alunoDOMAIN);
	}
}
