package br.ucsal.bes20181.poo.organizadorTurmas.DOMAIN;

public class TurmaDOMAIN {

	private String materia;
	private Integer qtdAlunos;
	private Boolean DispProfessor;
	private String dataInicio;
	private String dataFim;
	private Integer numTurma;
	
	public TurmaDOMAIN(String materia, Integer qtdMinAlunos, Boolean dispProfessor,String dataInicio, String dataFim, Integer numTurma) {
		this.materia = materia;
		this.qtdAlunos = qtdMinAlunos;
		this.DispProfessor = dispProfessor;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.numTurma = numTurma;
	}
	
	public Integer getNumTurma() {
		return numTurma;
	}

	public void setNumTurma(Integer numTurma) {
		this.numTurma = numTurma;
	}

	public String getMateria() {
		return materia;
	}

	public void setMateria(String materia) {
		this.materia = materia;
	}

	public Integer getQtdAlunos() {
		return qtdAlunos;
	}

	public void setQtdAlunos(Integer qtdMinAlunos) {
		this.qtdAlunos = qtdMinAlunos;
	}

	public Boolean getDispProfessor() {
		return DispProfessor;
	}

	public void setDispProfessor(Boolean dispProfessor) {
		DispProfessor = dispProfessor;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	
}