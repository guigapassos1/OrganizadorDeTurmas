package br.ucsal.bes20181.poo.organizadorTurmas.DOMAIN;

import br.ucsal.bes20181.poo.organizadorTurmas.TUI.AlunoTUI;
import br.ucsal.bes20181.poo.organizadorTurmas.TUI.ProfessorTUI;
import br.ucsal.bes20181.poo.organizadorTurmas.TUI.TurmaTUI;

public class MainActivity {

	static AlunoTUI aluno = new AlunoTUI();
	static ProfessorTUI professor = new ProfessorTUI();
	TurmaTUI turma = new TurmaTUI();
	
	public static void main(String[] args) {	
		aluno.alunoRegistro();
		//professor.ProfessorRegistro();
		//turma.TurmaRegistro();
	}
}