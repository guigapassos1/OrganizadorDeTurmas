package br.ucsal.bes20181.poo.organizadorTurmas.DOMAIN;

public class ProfessorDOMAIN {

	String nome;
	String email;
	String materia;

	public ProfessorDOMAIN(String nome, String email, String materia) {
		this.nome = nome;
		this.email = email;
		this.materia = materia;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMateria() {
		return materia;
	}

	public void setMateria(String materia) {
		this.materia = materia;
	}

}
