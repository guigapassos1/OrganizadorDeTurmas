package br.ucsal.bes20181.poo.organizadorTurmas.BO;

import java.util.Scanner;

import br.ucsal.bes20181.poo.organizadorTurmas.DOMAIN.AlunoDOMAIN;

public class AlunoBO {
	
	Scanner sc = new Scanner(System.in);
	
	public void verificarNome(AlunoDOMAIN alunoReceber) {
		if(!alunoReceber.getNome().toUpperCase().matches("^[A-Z]+$")) {
			System.out.println("Somente letras");
			System.out.println("Nome do Aluno:");
			alunoReceber.setNome(sc.next());
			verificarNome(alunoReceber);
		}
	}
	public void verificarEmail(AlunoDOMAIN alunoReceber) {
		if(!alunoReceber.getEmail().toUpperCase().matches("^[A-Z@.]+$")) {
			System.out.println("Somente e-mail");
			System.out.println("E-mail do Aluno:");
			alunoReceber.setEmail(sc.next());
			verificarEmail(alunoReceber);
		}
	}
	
}